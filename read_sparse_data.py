from collections import defaultdict, Counter
from scipy.sparse import dok_matrix
import numpy as np
import pandas as pd
import time
import xml.etree.ElementTree as ET
import pickle as pkl

def read_css(path,min_links = 1,  cosine_threshold=0.6):

    ids = defaultdict(int)
    count_als = defaultdict(int)
    entries = 0
    for index,line in enumerate(open(path, "r")):
        if index ==0:
            continue
        id = line.split("\t")[0]
        count_als[id] += 1
        if id not in ids:
                entries += 1
                ids[id] = entries-1

    ids2 = {}
    ids2count = 0
    for index, ( (k,v), (k2, v2) ) in enumerate(zip(ids.items(), count_als.items())):
        if v2 >= min_links:
            ids2count += 1
            ids2[k] = ids2count -1
    ids = ids2
    p = len(ids)
    edgesdok = dok_matrix((p, p), dtype=np.float32)
    edgesdok = dok_matrix((p, p), dtype=np.float32)
    for index,line in enumerate(open(path, "r")):
        q = line.split("\t")[0]
        s = line.split("\t")[1]
        if q not in ids or s not in ids:
            continue

        distance = 1-float(line.split("\t")[4])
        edgesdok[ids[q],ids[s]] = distance
        edgesdok[ids[s],ids[q]] = distance
    edgesdok.setdiag(0)
    return((ids, edgesdok))

    
    return(edgesdok)

def read_blast(path, min_links = 10):
    ids = defaultdict(int)
    count_als = defaultdict(int)
    entries = 0
    cache = []
    for index,line in enumerate(open(path, "r")):
        cache.append((line.split("\t")[1], line.split("\t")[2], float(line.split("\t")[11])))
        id = line.split("\t")[1]
        count_als[id] += 1
        if id not in ids:
                entries += 1
                ids[id] = entries-1
                
    #print(Counter(count_als.values()))
    #print(cache[0:10])
    lengths = len(ids)
    ids2 = {}
    ids2count = 0
    for index, ( (k,v), (k2, v2) ) in enumerate(zip(ids.items(), count_als.items())):
        if v2 >= min_links:
            ids2count += 1 
            ids2[k] = ids2count -1
    ids = ids2
    p = len(ids)
    filtered_lengths = p

    edgesdok = dok_matrix((p, p), dtype=np.float32)
    
    for (q, s, d) in cache:
        if q not in ids or s not in ids:
            continue
        distance = 1-d/100
        edgesdok[ids[q],ids[s]] = distance
        edgesdok[ids[s],ids[q]] = distance
    
    edgesdok.setdiag(0)
    print("matrix size before filtering %s, after filtering %s, filtering by min_links %s" % (lengths, filtered_lengths, str(min_links)))
    return((ids, edgesdok))

def read_graphml(path, min_links = 10):
    xmldata = open(path, 'r').read()
    root = ET.fromstring(xmldata)
    print(root.findall("graph"))

#ids, mat = read_blast("../allvall_filter.tsv")    
#read_graphml("../../di_edgelist.graphml")
#(names, dist) = read_blast("viral_genomes_vs_viral_genomes_strict.tsv")
#npdist = dist.todense()
#pd.DataFrame(npdist, index = names, columns = names).to_csv("viral_blast.csv")

