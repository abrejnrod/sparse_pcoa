import numpy as np
from numpy import ones
from scipy.sparse import dok_matrix, csc_matrix, eye, identity
from scipy.sparse.linalg import eigsh
import pandas as pd

def center(D):
    """
    Double enter a distance matrix D - TODO:sparse versions of the mat construction
    """
    
    D = csc_matrix(D)
    n = D.shape[0]
    I = identity(n, format = "dok")
    one = dok_matrix((n,n))
    for i in range(n):
        for j in range(n):
            one[i,j] = 1

    mat = one - I/n
     
    delta = mat*D*mat
    return(D)
    
def sparse_pcoa(D, k = 2):
    A = -0.5*D.power(2)
    delta = center(A)
    w, v = eigsh(A, k = k)
    return(w, v)

    
#dat = pd.read_csv("../../small_css_matrix.tsv", sep = "\t", index_col = 0)
#D = dok_matrix(dat)
#e = sparse_pcoa(D)

