from read_sparse_data import read_blast
from pcoa import sparse_pcoa
import pickle
import argparse
import time 
def run_pcoa(inputfile, outputfile):
    t = time.process_time()
    (names, dist) = read_blast(inputfile)
    print(names)
    print(dist)
    elapsed_time = time.process_time() - t
    t = time.process_time()
    (w, v)  = sparse_pcoa(dist)
    elapsed_time_pcoa = time.process_time() - t
    print("done, time to read: %s, time to pcoa: %s" % (str(elapsed_time),str(elapsed_time_pcoa)))
    pickle.dump((w, v, names, dist), open(outputfile, "wb"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Read all vs all blast tabular output and do PCoA')
    parser.add_argument('-i', '--inputfile', type=str, help='Input file path')
    parser.add_argument('-o', '--outputfile', type=str, help='Output file path')
    args = parser.parse_args()
    run_pcoa(args.inputfile, args.outputfile)
