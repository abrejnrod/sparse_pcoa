#!/usr/bin/env python3

import numpy as np
import random
import skbio
from pcoa import sparse_pcoa
from scipy.sparse import dok_matrix
import time
from itertools import product
import pandas as pd
import seaborn as sns

def symmetric_random_matrix(size = (3,3), threshold = 0.1, sparsity = 0.9):
    rng = np.random.RandomState(123)
    X = np.random.lognormal(size = size)
    X = X / X.max()
    sample = np.random.choice([True, False], size, p = (sparsity, 1 - sparsity))
    X[sample] = 0
    X  = np.triu(X)
    X = X + X.T - np.diag(np.diag(X))
#    X[X < 0.6] = 0
    np.fill_diagonal(X, 0)
    return(X)

results = pd.DataFrame(columns=['N','Sparsity','Function','Time', 'Nonzero'])
#for i in product(range(100, 500, 100), list(np.arange(0, 1, 0.5)) ):
#for i in product(range(1000, 10000, 3000), [0.1, 0.99] ):
for i in product([1000, 10000, 50000, 100000], [0.1, 0.99] ):    
    n = i[0]
    s = i[1]
    X = symmetric_random_matrix(size = (n,n), sparsity = s)
    nz = np.count_nonzero(X)
    start_skbio = time.time()
    #X_skbio_pcoa = skbio.stats.ordination.pcoa(X, number_of_dimensions = 2)
    pass
    end_skbio  = time.time() - start_skbio
    X_sparse = dok_matrix(X)
    start_sparse_pcoa = time.time()
    X_sparse_pcoa = sparse_pcoa(X_sparse, k = 2)
    end_sparse_pcoa  = time.time() - start_sparse_pcoa
    results = results.append({"N": n, "Sparsity": s, "Function": "skbio", "Time" : end_skbio, "Nonzero": nz}, ignore_index = True)
    results = results.append({"N": n, "Sparsity": s, "Function": "sparse_pcoa", "Time" : end_sparse_pcoa, "Nonzero" : nz}, ignore_index = True)
    print(results)

#print(results)
sns.set(color_codes=True)
results['Sparsity'] = pd.Categorical(results['Sparsity'])
ax = sns.lineplot(x="N", y="Time",hue="Sparsity", style="Function",data=results)
import matplotlib.pyplot as plt
plt.show()
